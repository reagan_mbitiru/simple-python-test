import pytest
from simple import a
from simple import b


# -- without test explanation
def test_function_a():
    assert a() == 3


# -- with test explanation
def test_function_b():
    assert b() == 3, "value should have been 3"


# -- test exception
def test_zero_division_a():
    with pytest.raises(ZeroDivisionError):
        1 / 0


# -- with exception message
def test_zero_division_b():
    with pytest.raises(ZeroDivisionError, message="Expecting ZeroDivisionError"):
        17/0


# -- context specific comparison
def test_set_comparison():
    set1 = set("1308")
    set2 = set("1308")
    assert set1 == set2
